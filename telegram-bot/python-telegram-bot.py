#importazione librerie file
import json
import tarfile
#importazione librerie varie
import logging
import os
import platform
import wave
from difflib import SequenceMatcher
from threading import Event
import numpy as np
import wget
#importazione librerie OCR
from PIL import Image
import pytesseract
import argparse
import cv2
#importazione libreria deepspeech
from deepspeech import Model
#importazione librerie ibm
from ibm_cloud_sdk_core.authenticators import IAMAuthenticator
from ibm_watson import ApiException
from ibm_watson import AssistantV2
#importazione librerie Telegram
from telegram import InlineKeyboardButton, InlineKeyboardMarkup
from telegram.ext import CallbackQueryHandler
from telegram.ext import CommandHandler
from telegram.ext import MessageHandler, Filters
from telegram.ext import Updater

exit = Event()
keys = {}
intents = {}

#controlla se ci sono i tre file "alphabet.txt", "scorer", "output_graph.tflite" e "output_graph.pbmm" nel caso ritorna true altrimenti false
def check_for_files():
    return True if os.path.isfile("alphabet.txt") and os.path.isfile("scorer") and (os.path.isfile("output_graph.pbmm") or os.path.isfile("output_graph.tflite")) else False

#controlla se c'e il mdello deepspeech
def check_for_xz():
    return True if (os.path.isfile("transfer_model_tensorflow_it.tar.xz") or os.path.isfile("transfer_model_tflite_it.tar.xz")) else False


#Scarica il modello italiano di DeepSpeech con un wget in base alla piattaforma
def download_model():
    print("Downloading Italian model...")
    print("Sto scaricando il modello per: " + platform.machine())
    if platform.machine() != "armv7l":
        wget.download(
            "https://github.com/MozillaItalia/DeepSpeech-Italian-Model/releases/download/2020.08.07"
            "/transfer_model_tensorflow_it.tar.xz",
            'transfer_model_tensorflow_it.tar.xz')
    else:
        wget.download(
            "https://github.com/MozillaItalia/DeepSpeech-Italian-Model/releases/download/2020.08.07"
            "/transfer_model_tflite_it.tar.xz",
            'transfer_model_tflite_it.tar.xz')

#estrae dall' archivio "model_tensorflow_it.tar.xz" o "transfer_model_tflite_it.tar.xz" i file
def untar():
    print("\nUnzipping model...")
    tar = None
    if os.path.exists("transfer_model_tensorflow_it.tar.xz"):
        tar = tarfile.open("transfer_model_tensorflow_it.tar.xz")
    elif os.path.exists("transfer_model_tflite_it.tar.xz"):
        tar = tarfile.open("transfer_model_tflite_it.tar.xz")
    tar.extractall()
    tar.close()

#controlla se c'e gia il modello DeepSpeech
def check_for_ds():
    """
    Check if Deepspeech is present or not. If not it downloads Italian deepspeech model.
    :return:
    """

    if not os.path.exists('DS'):
        os.makedirs('DS')

    os.chdir("DS")

    flg_files = check_for_files()
    flg_xz = check_for_xz()

    if not flg_xz and not flg_files:
        download_model()
        untar()
        if os.path.exists("transfer_model_tensorflow_it.tar.xz"):
            os.remove("transfer_model_tensorflow_it.tar.xz")
        elif os.path.exists("transfer_model_tflite_it.tar.xz"):
            os.remove("transfer_model_tflite_it.tar.xz")

    elif flg_xz:
        untar()
        if os.path.exists("transfer_model_tensorflow_it.tar.xz"):
            os.remove("transfer_model_tensorflow_it.tar.xz")
        elif os.path.exists("transfer_model_tflite_it.tar.xz"):
            os.remove("transfer_model_tflite_it.tar.xz")

    os.chdir("..")
    return True


#prende gli intenti e le chiavi di accesso alle API
def load_config():
    with open("../skill_exporter/json_skill.json") as file:
        ris = json.load(file)
        ris = ris["intents"]
        intents = ris
        #print(json.dumps(intents, indent=2))
        file.close()
    with open("keys.cfg") as myfile:
        for line in myfile:
            name, var = line.partition("=")[::2]
            keys[name.strip()] = var[:-1]
        myfile.close()

    return intents

    #print(keys)

#controlla se le stringhe a e b sono simili
def similar(a, b):
    """
    Check if two strings are similar with builtins.
    :param a: first string
    :param b: second string
    :return: coefficient of similarity
    """

    return SequenceMatcher(None, a, b).ratio()

def build_menu(buttons,n_cols,header_buttons=None,footer_buttons=None):
  menu = [buttons[i:i + n_cols] for i in range(0, len(buttons), n_cols)]
  if header_buttons:
    menu.insert(0, header_buttons)
  if footer_buttons:
    menu.append(footer_buttons)
  return menu

def message_handler(messaggio, update, context):
    session_id, assistant = authenticate()

    message = assistant.message(
        keys['Assistant_ID'],
        session_id,
        input={'text': messaggio},
        context={}).get_result()

    try:
        response = assistant.delete_session(
            assistant_id=keys['Assistant_ID'],
            session_id=session_id
        ).get_result()
    except ApiException as ex:
        print("Method failed with status code " + str(ex.code) + ": " + ex.message)

    if len(message["output"]["generic"]) > 0:

        if message["output"]["generic"][0]["response_type"] == "suggestion":
            btns = []
            #print(json.dumps(intents, indent=2))
            #print("\n\n\n\n\n\n\n")
            #print(json.dumps(message["output"]["generic"][0]["suggestions"], indent=2))
            for suggestion in message["output"]["generic"][0]["suggestions"]:
                buffer = False
                for intent in intents:
                    #print("INTENT FOR")
                    for text in intent["examples"]:
                        #print("INTENT:" + intent["intent"])
                        #print("INTENT2:" + suggestion["value"]["input"]["intents"][0]["intent"])
                        #if intent["intent"] == suggestion["value"]["input"]["intents"][0]["intent"]:
                            #print("intent")
                        if (float(similar(text["text"], messaggio)) >= 0.5 and
                        len(suggestion["value"]["input"]["intents"]) > 0
                        and intent["intent"] == suggestion["label"]):

                            btns.append(InlineKeyboardButton(text["text"], callback_data = text["text"]))
                            buffer = True
                if buffer == False:
                    btns.append(InlineKeyboardButton(suggestion["label"], callback_data = suggestion["label"]))


            reply_markup=InlineKeyboardMarkup(build_menu(btns,n_cols=1))
            context.bot.send_message(chat_id=update.effective_chat.id, text="Intendevi dire:",reply_markup=reply_markup)
        else:
            if message["output"]["intents"][0]["confidence"] >= 0.5:
                for messaggio in message["output"]["generic"]:
                    context.bot.send_message(chat_id=update.effective_chat.id, text=messaggio["text"], parse_mode='HTML')
            else:
                btns = []
                #print(json.dumps(intents, indent=2))
                #print("\n\n\n\n\n\n\n")
                #print(json.dumps(message["output"]["generic"][0]["suggestions"], indent=2))

                for intent in intents:
                    #print("INTENT FOR")
                    for text in intent["examples"]:
                        #print("INTENT:" + intent["intent"])
                        #print("INTENT2:" + suggestion["value"]["input"]["intents"][0]["intent"])
                        #if intent["intent"] == suggestion["value"]["input"]["intents"][0]["intent"]:
                            #print("intent")
                        if float(similar(text["text"], messaggio)) >= 0.5:

                            btns.append(InlineKeyboardButton(text["text"], callback_data = text["text"]))
                if len(btns) > 0:
                    btns.append(InlineKeyboardButton("Nessuno dei precedenti", callback_data = "Nessuna delle precedenti"))
                    reply_markup=InlineKeyboardMarkup(build_menu(btns,n_cols=1))
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Intendevi dire:",reply_markup=reply_markup)
                else:
                    context.bot.send_message(chat_id=update.effective_chat.id, text="Non abbiamo trovato quello che cercavi!", parse_mode='HTML')

    else:
        btns = []
        #print(json.dumps(intents, indent=2))
        #print("\n\n\n\n\n\n\n")
        #print(json.dumps(message["output"]["generic"][0]["suggestions"], indent=2))

        for intent in intents:
            #print("INTENT FOR")
            for text in intent["examples"]:
                #print("INTENT:" + intent["intent"])
                #print("INTENT2:" + suggestion["value"]["input"]["intents"][0]["intent"])
                #if intent["intent"] == suggestion["value"]["input"]["intents"][0]["intent"]:
                    #print("intent")
                if float(similar(text["text"], messaggio)) >= 0.5:

                    btns.append(InlineKeyboardButton(text["text"], callback_data = text["text"]))

        if len(btns) > 0:
            btns.append(InlineKeyboardButton("Nessuno dei precedenti", callback_data = "Nessuno dei precedenti"))

            reply_markup=InlineKeyboardMarkup(build_menu(btns,n_cols=1))
            context.bot.send_message(chat_id=update.effective_chat.id, text="Intendevi dire:",reply_markup=reply_markup)
        else:
            context.bot.send_message(chat_id=update.effective_chat.id, text="Non abbiamo trovato quello che cercavi!", parse_mode='HTML')




#def bot_telegram(session_id, ds, assistant):
def bot_telegram(ds, intents):
    updater = Updater(token=keys["Telegram_key"])
    #bot = telegram.Bot(token=keys["Telegram_key"])
    dispatcher = updater.dispatcher
    '''
    message = assistant.message(
        keys['Assistant_ID'],
        session_id,
        input={'text': "Buongiorno"},
        context={}).get_result()
    '''
    logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

    def start(update, context):
        session_id, assistant = authenticate()

        message = assistant.message(
            keys['Assistant_ID'],
            session_id,
            input={'text': "Buongiorno"},
            context={}).get_result()
        #print(message)
        try:
            response = assistant.delete_session(
                assistant_id=keys['Assistant_ID'],
                session_id=session_id
            ).get_result()
        except ApiException as ex:
            print("Method failed with status code " + str(ex.code) + ": " + ex.message)

        for messaggio in message["output"]["generic"]:
            context.bot.send_message(chat_id=update.effective_chat.id, text=messaggio["text"], parse_mode='HTML')
        #messaggio = message["output"]["generic"][0]["text"] + "\n" + message["output"]["generic"][1]["text"]
        #context.bot.send_message(chat_id=update.effective_chat.id, text=messaggio)

    start_handler = CommandHandler('start', start)
    dispatcher.add_handler(start_handler)

    def echo(update, context):
        messaggio = ""
        tipo = ""
        if not update.message.text:
            if os.path.isfile('aud.ogg'):
                os.remove("aud.ogg")
            if os.path.isfile('aud.wav'):
                os.remove("aud.wav")
            if os.path.isfile('img.jpg'):
                os.remove("img.jpg")
            if update.message.voice:
                file = update.message.voice.get_file()
                tipo = "audio"
            elif update.message.audio:
                file = update.message.audio.get_file()
                tipo = "audio"
            elif update.message.photo:
                file = update.message.photo[0].get_file()
                tipo = "immagine"

            if tipo == "audio":
                file.download("aud.ogg")
                os.system("ffmpeg -i aud.ogg -ac 1 -ar 16000 aud.wav")
                fin = wave.open("aud.wav", 'rb')
                length = fin.getnframes()
                sr = 16000
                raw = fin.readframes(length)
                int16 = np.frombuffer(raw, dtype=np.int16)

                #QUI NON SI VERIFICA IL PROBLEMA
                messaggio = ds.stt(int16)
            elif tipo == "immagine":
                file.download("img.jpg")
                messaggio = OCR().splitlines()[0]
                #provaoutput = open("out", "a")
                #provaoutput.write(OCR())
                #provaoutput.close()

        else:
            messaggio = update.message.text
        print(messaggio)

        message_handler(messaggio, update, context)


    def suggestion_callback(update, context):
        query = update.callback_query
        messaggio = query.data
        print(messaggio)
        context.bot.send_message(chat_id=update.effective_chat.id, text=messaggio, parse_mode='HTML')

        if messaggio == "Nessuno dei precedenti":
            context.bot.send_message(chat_id=update.effective_chat.id, text="Ci dispiace tu non abbia trovato la risposta che cercavi!", parse_mode='HTML')
            return

        message_handler(messaggio, update, context)

    updater.dispatcher.add_handler(CallbackQueryHandler(suggestion_callback))



    echo_handler = MessageHandler((~Filters.command), echo)
    dispatcher.add_handler(echo_handler)

    def unknown(update, context):
        context.bot.send_message(chat_id=update.effective_chat.id, text="Comando non riconosciuto")

    unknown_handler = MessageHandler(Filters.command, unknown)
    dispatcher.add_handler(unknown_handler)

    return updater

def OCR():
    # Converti l'immagine in scala grigi
    image = cv2.imread("img.jpg")
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = cv2.threshold(gray, 0, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)[1]
    os.remove("img.jpg")
    cv2.imwrite("img.jpg", gray)
    text = pytesseract.image_to_string(Image.open("img.jpg"), lang='ita')
    os.remove("img.jpg")
    return text

def quit(signo, _frame):
    print("Interrupted by %d, shutting down" % signo)
    #exit.set()

def start_everything(ds, intents):

    session_id, assistant = authenticate()
    updater = bot_telegram(ds, intents)
    updater.start_polling()
    updater.idle()
    #return updater, assistant, session_id

#autenticazione al servizio IBM
def authenticate():

    authenticator = IAMAuthenticator(keys['API_Key']) #API key
    assistant = AssistantV2(
        version='2016-09-24',
        authenticator=authenticator)
    assistant.set_service_url(keys['Login_service']) #Login service (https://cloud.ibm.com/apidocs/assistant/assistant-v-2#service-endpoint)

    #########################
    # Sessions
    #########################

    session = assistant.create_session(keys['Assistant_ID']).get_result() #Assistant ID
    session_id = session['session_id']

    #print(session)
    #json.dumps(session, indent=-2))
    return session_id, assistant

def main():
    intents = load_config()
    #print(json.dumps(intents, indent=2))
    if check_for_ds():

        if os.path.exists("DS/output_graph.pbmm"):
            ds = Model("DS/output_graph.pbmm")
        elif os.path.exists("DS/output_graph.tflite"):
            ds = Model("DS/output_graph.tflite")

        #assistant = None
        #session_id = None

        '''
        while not exit.is_set():

            updater, assistant, session_id = start_everything(ds, intents, session_id, assistant)
            exit.wait(360)
            updater.stop()

        updater.stop()
        response = assistant.delete_session(
            assistant_id=keys['Assistant_ID'],
            session_id=session_id
        ).get_result()

        '''

        #updater, assistant, session_id = start_everything(ds, intents, session_id, assistant)
        start_everything(ds, intents)

        #while not exit.is_set():
        #    exit.wait(10)

        #updater.stop()

        if os.path.isfile("aud.wav"):
            os.remove("aud.wav")
        if os.path.isfile("aud.ogg"):
            os.remove("aud.ogg")
        if os.path.isfile("img.jpg"):
            os.remove("img.jpg")
        print("Sessione chiusa!")


if __name__ == '__main__':

    #import signal
    #for sig in ('TERM', 'HUP', 'INT'):
    #    signal.signal(getattr(signal, 'SIG'+sig), quit);

    main()


#sos
