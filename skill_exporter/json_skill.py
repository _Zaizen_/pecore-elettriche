#libreria per la gestione dei file json
import json

array_finale = {}
intents = []#Domande
dialog_nodes = []#Risposte

def append_intent(intent):
  if not "intent" in intent or not "examples" in intent:
    print("[I] \"intent\" non valido!")
    return
  intent_template = {
    "intent": intent["intent"],
    "examples":intent["examples"]
  }

  if "description" in intent:
    intent_template["description"] = intent["description"]

  intents.append(intent_template)

def append_dialog_node(dialog_node):
  if not "title" in dialog_node:
    print("[I] Formato del dialog_node errato: manca la chiave \"title\"!!")
    return

  dialog_node_template = {
    "type":"standard",
    "title": dialog_node["title"],
    "output":
      {
        "generic":[
          {
            "values": dialog_node["output"]["generic"][0]["values"],
            "response_type":"text",
            "selection_policy":"sequential"
          }
        ]
      },
    "dialog_node": dialog_node["title"]
  }

  if len(dialog_node["conditions"]) <= 0:
    dialog_node_template["conditions"] = "#" + dialog_node["title"]
  else:
    dialog_node_template["conditions"] = dialog_node["conditions"]

  if "next_step" in dialog_node:
      if "behavior" in dialog_node["next_step"] and "selector" in dialog_node["next_step"] and "dialog_node" in dialog_node["next_step"]:
        dialog_node_template["next_step"] = {
          "behavior": dialog_node["next_step"]["behavior"],
          "selector": dialog_node["next_step"]["selector"],
          "dialog_node": dialog_node["next_step"]["dialog_node"]
        }
      else:
        print("[E] Errore")

  if "selection_policy" in dialog_node["output"]["generic"][0]:
    dialog_node_template["output"]["generic"][0]["selection_policy"] = dialog_node["output"]["generic"][0]["selection_policy"]

  if len(dialog_nodes) - 1 >= 0:
    if not "previous_sibling" in dialog_node_template:
      dialog_node_template["previous_sibling"] = ""
    if dialog_nodes[len(dialog_nodes) - 1]["title"] != dialog_node_template["previous_sibling"]:
      dialog_node_template["previous_sibling"] = dialog_nodes[len(dialog_nodes) - 1]["title"]

  dialog_nodes.append(dialog_node_template)

def generate(file, input_tag, output_tag, category_tag = None):
  file_name_start = file.name.rfind("/") + 1
  if file_name_start < 0:
    file_name_start = 0

  file_name = file.name[file_name_start:len(file.name)]
  print("[I] Generazione di {0:s}...".format(file_name))
  lines = file.readlines()
  index = 0
  category = ""

  while index < len(lines) - 1:
      if lines[index][:1] != '\n':
          category = ""

          examples = []#Domande

          #Categoria
          while index < len(lines) and lines[index][:2] != input_tag and lines[index][:2] != output_tag:
              if lines[index][:2] != '\n':
                  line = lines[index]
                  line = line[2:]
                  line = line[:-1]
                  examples.append({"text":line.replace(" ", "_")})
                  category = line.replace(" ", "_")

              index = index + 1

          while index < len(lines) and lines[index][:2] != output_tag:
              if lines[index][:2] != '\n':
                  line = lines[index]
                  line = line[2:]
                  line = line[:-1]
                  examples.append({"text":line})
                  if(len(category) <= 0): #Usa la domanda come categoria se la categoria non è presente
                    category = line.replace(" ", "_")

              index = index + 1

          append_intent(
            {
              "intent":(category),
              "examples":examples
            }
          )

          values = []
          while index < len(lines) and ((lines[index][:2] != input_tag) and (lines[index][:2] != category_tag)):
              if lines[index][:2] != '\n':
                  line = lines[index]
                  line = line[2:]
                  line = line[:-1]
                  values.append({"text":line})
              index = index + 1

          append_dialog_node(
          {
            "type":"standard",
            "title":category,
            "output":
              {
                "generic":[
                  {
                    "values":values,
                    "response_type":"text",
                    "selection_policy":"sequential"
                  }
                ]
              },
            "conditions":"#"+category,
            "dialog_node": category
          }
          )

  print("[I] Generazione completata!\n")

def import_generic(file):
  file_name_start = file.name.rfind("/") + 1
  if file_name_start < 0:
    file_name_start = 0

  file_name = file.name[file_name_start:len(file.name)]
  print("[I] Caricamento di {:s}...".format(file_name))

  content = json.loads(file.read())

  if("dialog_nodes" in content): #dialog_nodes
    loaded_dialog_nodes = content["dialog_nodes"]
    for dialog_node in loaded_dialog_nodes:
      append_dialog_node(dialog_node)

  if("intents" in content): #intents
    loaded_intents = content["intents"]
    for intent in loaded_intents:
      append_intent(intent)

  if(not "dialog_nodes" in content and not "intents" in content):
    print("[E] Formato file non valido!")
    return

  print("[I] Caricamento completato\n")

def main():
  before_intents = open("hardcoded_intents.json", 'r', encoding='UTF-8')
  import_generic(before_intents)

  before_dialog_nodes = open("dialog_before.json", 'r', encoding='UTF-8')
  import_generic(before_dialog_nodes)

  faq = open("faq.txt", 'r', encoding='UTF-8')
  generate(faq, 'Q ', 'A ', 'C ')

  glo = open("glo.txt", 'r', encoding='UTF-8')
  generate(glo, 'G ', 'D ', None)

  after_dialog_nodes = open("dialog_after.json", 'r', encoding='UTF-8')
  import_generic(after_dialog_nodes)

  intent_cat_index = 0
  dialog_nodes_cat_index = 0

  intent_categories = []
  dialog_nodes_categories = []

  intent_dups = []
  dialog_node_dups = []

  global intents
  global dialog_nodes

  print("[I] Controllo dei duplicati...")
  for item in intents:
    intent_categories.append(item["intent"])

  for item in dialog_nodes:
    dialog_nodes_categories.append(item["title"])

  #Cerca categorie duplicate
  for item in intent_categories:
    if(intent_categories.count(item) > 1):
      intent_dups.append({"item": item, "index": intent_cat_index})
    intent_cat_index = intent_cat_index + 1

  for item in dialog_nodes_categories:
    if(dialog_nodes_categories.count(item) > 1):
      dialog_node_dups.append({"item": item, "index": dialog_nodes_cat_index})
    dialog_nodes_cat_index = dialog_nodes_cat_index + 1

  if(len(intent_dups) <= 0 and len(dialog_node_dups) <= 0):
    print("    [I] Nessun duplicato trovato!\n")
  else:
    #ordina i duplicati per categoria per evitare disastri
    intent_dups = sorted(intent_dups, key = lambda k: k['item'])
    dialog_node_dups = sorted(dialog_node_dups, key = lambda k: k['item'])

    intent_cur_sublist_item = intent_dups[0]["item"]
    dialog_node_cur_sublist_item = dialog_node_dups[0]["item"]

    intent_cur_sublist_item_index = intent_dups[0]["index"]
    dialog_node_cur_sublist_item_index = dialog_node_dups[0]["index"]

    intents_to_remove = []
    dialog_nodes_to_remove = []

    #comincia a 1 anziche a 0 per saltare il controllo sulla prima voce che, oltre ad essere inutile, sarebbe disastroso
    for j in range(1, len(intent_dups)):
      if(intent_cur_sublist_item != intent_dups[j]["item"]):
        intent_cur_sublist_item = intent_dups[j]["item"]
        intent_cur_sublist_item_index = intent_dups[j]["index"]
        #Se cambia la categoria salto la prima voce
        continue

      index = intent_dups[j]["index"]
      print("    [I] \"intent\" duplicato trovato: " + intents[intent_cur_sublist_item_index]["intent"])

      #Unisce tutti gli examples dei duplicati in quello della prima occorrenza delle categoria
      for new in intents[index]["examples"]:
        if(not new in intents[intent_cur_sublist_item_index]["examples"]):
          intents[intent_cur_sublist_item_index]["examples"].append(new)

      intents_to_remove.append(index)

    for j in range(1, len(dialog_node_dups)):
      if(dialog_node_cur_sublist_item != dialog_node_dups[j]["item"]):
        dialog_node_cur_sublist_item = dialog_node_dups[j]["item"]
        dialog_node_cur_sublist_item_index = dialog_node_dups[j]["index"]
        #Se cambia la categoria salto la prima voce
        continue

      index = dialog_node_dups[j]["index"]
      print("    [I] \"dialog_node\" duplicato trovato: " + dialog_nodes[dialog_node_cur_sublist_item_index]["title"])

      #Unisce tutti i text dei duplicati in quello della prima occorrenza delle categoria
      for new in dialog_nodes[index]["output"]["generic"][0]["values"]:
        if(not new in dialog_nodes[dialog_node_cur_sublist_item_index]["output"]["generic"][0]["values"]):
          dialog_nodes[dialog_node_cur_sublist_item_index]["output"]["generic"][0]["values"].append(new)

      dialog_nodes_to_remove.append(index)

    #Rimuove tutti i duplicati tranne la prima occorrenza
    intents = [i for j, i in enumerate(intents) if j not in intents_to_remove]
    print("    [I] \"intent\" duplicati fusi e rimossi!")

    #Sistema i nodi fratelli
    for i in enumerate(dialog_nodes):
      if(i[0] in dialog_nodes_to_remove):
        new_sibiling = i[1]["previous_sibling"]
        if(i[0] + 1 < len(dialog_nodes)):
          dialog_nodes[i[0] + 1]["previous_sibling"] = new_sibiling

    #Rimuove tutti i duplicati tranne la prima occorrenza
    dialog_nodes = [i for j, i in enumerate(dialog_nodes) if j not in dialog_nodes_to_remove]
    print("    [I] \"dialog_nodes\" duplicati fusi e rimossi!")
    print("    [I] Tutti i duplicati sono stati fusi e rimossi!\n")

  array_finale["intents"] = intents
  array_finale["entities"] = []
  array_finale["metadata"] = {
    "api_version": {
      "major_version": "v2",
      "minor_version": "2018-11-08"
    },
    "from-sample": True
  }

  array_finale["dialog_nodes"] = dialog_nodes
  array_finale["counterexamples"] = []
  array_finale["system_settings"] = {
    "disambiguation": {
      "prompt": "Intendevi dire:",
      "enabled": True,
      "randomize": True,
      "max_suggestions": 5,
      "suggestion_text_policy": "title",
      "none_of_the_above_prompt": "Nessuno dei precedenti"
    },
      "system_entities": {
      "enabled": True
    },
      "human_agent_assist": {
      "prompt": "Intendevi dire:"
    }
  }
  array_finale["learning_opt_out"] = False
  array_finale["name"] = "My first skill"
  array_finale["language"] = "it"
  array_finale["description"] = ""

  with open('json_skill_pretty.json', 'w', encoding='UTF-8') as out_file:
    json.dump(array_finale, out_file, indent = 4)

  with open('json_skill.json', 'w', encoding='UTF-8') as outfile:
    json.dump(array_finale, outfile)

  print("[I] json_skill.json creato con successo!\n[I] La versione con indentazione si trova al file: json_skill_pretty.json")

main()
