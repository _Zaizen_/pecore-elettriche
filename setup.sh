sudo apt update;
sudo apt install git;
sudo curl -fsSL https://get.docker.com -o get-docker.sh;
sudo sh get-docker.sh;
sudo git clone https://gitlab.com/_Zaizen_/pecore-elettriche.git;
sudo cd pecore-elettriche;
sudo docker build --tag watson_bot:latest .;
sudo docker run --detach --name WatsonBot watson_bot:latest;
sudo docker volume create portainer_data;
sudo docker run -d -p 8000:8000 -p 9000:9000 --name=portainer --restart=always -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer-ce;
