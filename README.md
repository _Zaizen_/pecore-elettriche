<h1>Introduzione</h1>

Questa repo servirà a tener traccia dell'avanzamento del nostro lavoro con il chatbot creato grazie l'intelligenza artificiale Watson di IBM.<br>
I file caricati verranno spesso rimpiazzati quindi per risalire a certi file bisognerà andare indietro nella storia dei commit e recuperarli.

<h2>Avvertenza</h2>

Ciò che appare in questa repo potrebbe risultare offensivo ad alcune persone, se vi sentite presi in causa in ciò, ce ne scusiamo ma dovete capire che programmare non è sempre una passeggiata.<br>

Potrebbero esserci errrori di grammatica e/o sintassi in alcuni commenti e/o documentazione, adattatevi. Nei commenti non ci faremo problemi ad essere onesti e sinceri con voi, quindi se non scriviamo qualcosa o scriviamo sono stanco ecc. cercatevi a cosa serve.

<h1>Come installare la repo GitLab</h1>
[Guida su come avere la repo GitLab sul tuo dispositivo](https://gitlab.com/_Zaizen_/pecore-elettriche/-/blob/master/Documentazione/Guida_GIT.md)

<h1>Fase di avanscoperta</h1>
Abbiamo iniziato a guardare le fondamenta dell'intelligenza artificiale Watson.<br>
Per prima cosa abbiamo iniziato a leggere [questo articolo](https://www.ibm.com/blogs/watson/2017/03/bot-yourself/) e ci siamo registrati seguendo [questo link](https://cloud.ibm.com/registration?target=/catalog/services/conversation/).<br>
Finito di leggere il tutorial abbiamo anche trovato [questa risorsa](https://cloud.ibm.com/docs/assistanttopic=assistant-getting-started#getting-started-add-catalog) grazie alla quale si è poi riusciti a creare una prima demo di un chatbot abbastanza basico.

Vuoi provare la nostra primissima demo? [Clicca qui!](https://web-chat.global.assistant.watson.cloud.ibm.com/preview.html?region=eu-de&integrationID=050700d2-3405-48ea-951b-bd542c7205ff&serviceInstanceID=9d869b75-2233-4c35-937b-aef3f46b7c00)

<h1>Come interfacciare watson assistant con un bot telegram</h1>
Alcune delle fonti che abbiamo trovato utili:
- [Stackoverflow e la risposta di un utente](https://stackoverflow.com/a/53758239)
- [Uno dei link della risposta](https://github.com/data-henrik/watson-conversation-tool/)
- [Python SDK di Watson](https://github.com/watson-developer-cloud/python-sdk/)

<h2>Come installre l'SDK</h2>
Per prima cosa andremo ad installare l'SDK. Per fare ciò avremo bisogno di almeno [python3.5](https://www.python.org/downloads/).<br>
Dopo di che ci rechiamo nella cartella dedicata al progetto pecore-elettriche (la repo GitLab per intenderci) e qui installeremo l'sdk. Io vi consiglio di installarlo in modo dedicato alla repo con questi comandi:

Su linux:
`python3 -m venv env`

Su win:
`py -m venv env`

Attiviamo l'ambiente virtuale in cui installare l'SDK:

Linux:
`source env/bin/activate`

Win:
`.\env\Scripts\activate`

Installiamo l'SDK:
`pip install --upgrade ibm-watson`

Se su linux vi da problemi con i permessi provate ad usare questo comando:
`sudo -H pip install --ignore-installed six ibm-watson`

In caso di altri problemi consultate: [Watson Developer Cloud Python SDK](https://github.com/watson-developer-cloud/python-sdk)

Abbandoniamo l'ambiente virtuale:
`deactivate`

Bene ora abbiamo installato i moduli necessari all'uso di IBM Cloud con python.

<h1>Come autenticarsi</h1>
[Servizio IAM](https://github.com/watson-developer-cloud/python-sdk#iam)

<h1>Creazione di un nuovo progetto condiviso</h1>
file molto utile da seguire per la realizzazione di un chatbot con watson. [Qui il link](https://artificial-intelligence.unibs.it/didattica-IA/wp-content/uploads/Chatbot-With-Watson-v1.pdf)<br>
Guida aggiornata in confronto al link precedentemente menzionato sul come creare un chatbot con watson assistant, la si può consultare [qui](https://cloud.ibm.com/docs/assistant?topic=assistant-getting-started#getting-started-tutorial). <br>
Per creare un nuovo progetto: [Clicca qui!](https://eu-gb.dataplatform.cloud.ibm.com/projects/)

<h1>Machine learning</h1>
Su Watson Assistant c'è un modo per creare una machine learning dato uno specifico dataset in formato csv. Il modo in cui abbiamo fatto ciò è stato grazie all'uso di un programma in python che automaticamente grazie alle iniziali prima di domande e risposte crea il dataset.<br>
È stato aggiunto il file Hally2017.docx che farà da dataset.

<h1>Integrare l'assistente con telegram</h1>
Per integrare l'assistente con sistemi (come telegram) che non supportati di default bisogna farsi un programma dedicato usando l'api a disposizione: [qui](https://cloud.ibm.com/docs/assistant?topic=assistant-api-overview)

<h1>Generazione di skill in automatico</h1>

Tutorial sull'uso dello script json_skill.py:

- Nella stessa cartella dello script si mette il file con le domande e risposte su cui creare la skill
- Dentro lo script nelle prime righe si può scegliere il nome del file di input e si immette il file desiderato
- A fine del file si può scegliere su che file mettere l'output, se non esiste il file lo si deve creare a mano
- Si esegue lo script con:

  Su Linux:
    'python3 json_skill.py'
  Su Win:
    'py json_skill.py'

Lo script è già disponibile nella cartella [csv_creator](https://gitlab.com/_Zaizen_/pecore-elettriche/-/blob/master/csv_creator) insieme ad altri script per la creazione di file csv di vario tipo.
Anche il file JSON è già presente nella cartella se lo si vuole usare prefatto.

<h1>Importare la skill</h1>
Per importare il file JSON:

Andare su [IBM Cloud](https://cloud.ibm.com/)<br>
Andare su Dashboard > [Servizi](https://cloud.ibm.com/resources) >  Watson Assistant-** (dovete averlo attivato, nel caso si può attivare da [qui](https://cloud.ibm.com/catalog/services/watson-assistant))<br>
Dento il nuovo pannello che apre > Avvia Watson assistant<br>
Ora vi aprirà una nuova scheda. Sulla sinistra c'è un menù e la seconda voce si chiama Skills<br>
Fate Create skill > Dialog skill<br>
Nel nuovo pannello selezionate Import skill e selezionate il file JSON desiderato .<br>
Fate Import ed avete finito.

<h1>Script per comunicare con l'assistente, configurazione chiavi API</h1>
Dentro la cartella [telegram-bot](https://gitlab.com/_Zaizen_/pecore-elettriche/-/blob/master/telegram-bot) è presente il tutto con anche un esempio di un file di configurazione per le chiavi dell'API ecc...<br>
Per impostare le proprie chiavi bisogna creare un file **keys.cfg** dentro telegram-bot il quale non verrà mai aggiunto ai commit (almeno che non lo aggiungiate appositamente a mano per questioni di sicurezza) grazie alla nuova condizione nel .gitignore.<br>
Eseguendo il file ora come ora verrà fatta una domanda predefinita e si riceverà la risposta impostata.

<h1>Creazione del bot telegram</h1>
Per realizzarlo utilizzeremo [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot).
Il bot telegram è ora sempre online all'username: [@WatsonAssistantBladeRunnerbot](https://t.me/WatsonAssistantBladeRunnerbot), in caso non dovesse funzionare vuol dire che è in manutenzione.<br>

<h1>Integrazione DeepSpeech</h1>
È stata aggiunta la possibilità di fare domande al bot tramite audio grazie al modello [DeepSpeech di Mozilla](https://commonvoice.mozilla.org/).<br>Il codice è molto confusionario ma punto a pulirlo il prima possibile in modo tale che sia più comprensibile per chi vorrebbe solo usarlo. <br>Si ringrazia inoltre Dag7 il quale con la sua repo (https://github.com/dag7dev/another-one-the-game/blob/terminal/main.py) ha reso molto facile comprendere il funzionamento di deepspeech e dal quale ho preso alcune funzioni utili al programma. Mi ha fatto risparmiare un sacco di tempo quindi ci tenevo a nominarlo.
È stato aggiunto il supporto per il Raspberry Pi in quanto il modello di deepspeech che veniva usato di defualt non andava bene.<br>

<h1>Supporto a Docker</h1>
È stato aggiunto il supporto a Docker grazie a [questo file](https://gitlab.com/_Zaizen_/pecore-elettriche/-/blob/master/dockerfile).
È stato rilasciato il dockerfile per poter far funzionare il bot su qualunque sistema si voglia in modo semplice e veloce (<b>anche armhf</b>).
Ciò ha richiesto molto lavoro ma il gruppo ne otterrà un grande vantaggio grazie all'integrazione con [Portainer](https://www.portainer.io/) un'interfaccia web che permette di gestire i container docker con permessi e gruppi. <br>
<br>Cos'è un container docker?<br>
Per spiegarlo sarebbe necessario un talk dedicato ([il talk che é stato fatto durante il linux day quest'anno](https://video.linux.it/videos/watch/452436fe-9db4-47f5-9916-48c1107bb9c0)) ma ciò che posso dire è che una volta fatto andare il dockerfile correttamente, é una figata.
Dall'interfaccia di Portainer i membri del gruppo da oggi potranno facilmente avviare/spegnere/riavviare il bot e controllare tutte le funzioni.

<h2>Vuoi supportarci economicamente?</h2>
<b>I 5:</b> <a href="https://it.liberapay.com/I5/"><img src="https://i.imgur.com/4B2PxjP.png" alt="Support I 5"/></a>  

<br><br><br><div><a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.</div>
