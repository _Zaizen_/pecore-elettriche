# Abbiamo scelto un SO Debian https://hub.docker.com/_/debian/
FROM debian:buster-slim

#Parti essenziali per il funzionamento del bot, qui sotto sono principalmente tutte librerie python, ma possiamo notare ffmpeg che serve per convertire i file audio
RUN apt-get update -y && \
    apt-get install -y \
    apt-utils \
    python3-pip \
    python3-dev \
    ffmpeg \
    libzbar-dev \
    libzbar0 \
    gcc \
    libffi6 \
    libffi-dev \
    build-essential \
    libssl-dev \
    libilmbase-dev \
    libopenexr-dev \
    libgstreamer1.0-dev \
    libgtk-3-0 \
    libleptonica-dev \
    tesseract-ocr \
    libtesseract-dev \
    libgl1-mesa-glx \
    python3-opencv \
    cmake \
    g++ \
    make


#In questa fase si copiano i file dentro al container docker
COPY telegram-bot/requirements /tmp/telegram-bot/
COPY telegram-bot/python-telegram-bot.py /tmp/telegram-bot/
COPY skill_exporter/json_skill.json /tmp/skill_exporter/
COPY telegram-bot/ita.traineddata /usr/share/tesseract-ocr/4.00/tessdata/

#imposta la cartella dove il container docker lavorera
WORKDIR /tmp/telegram-bot/

#aggiorniamo lel librerie
RUN pip3 install --upgrade pip setuptools wheel
RUN pip3 install -r requirements

ENTRYPOINT [ "python3" ]

#esegue il bot
CMD [ "python-telegram-bot.py" ]
