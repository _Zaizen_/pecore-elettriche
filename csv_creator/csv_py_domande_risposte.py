import csv
file = open("faq.txt",'r')
lines = file.readlines()

row_list = [["domande","risposte"]]
index_domande = 1
index_inserimento = 1
#print(lines)
for line in lines:
    if line[:2] == 'Q ':
        line = line[2:]
        line = line[:-1]

        index_buffer = index_domande
        while lines[index_buffer][:2] != 'A ':
            index_buffer = index_buffer + 1

        index_risposte = 0
        while (index_buffer + index_risposte) < len(lines) and lines[index_buffer + index_risposte][:2] != 'Q ':
            if lines[index_buffer + index_risposte][:2] == 'A ':
                risposta = lines[index_buffer + index_risposte][2:]
                risposta = risposta[:-1]
                buffer = [line, risposta]
                row_list.insert(index_inserimento, buffer)
                index_inserimento = index_inserimento + 1

            index_risposte = index_risposte + 1

    index_domande = index_domande + 1

print(row_list)

with open('csv.csv', 'w', newline='') as file:
    writer = csv.writer(file)
    writer.writerows(row_list,)
