<h1>Guida all'uso di git</h1>
Nella seguente guida vi spiegherò le basi dell'uso di git. Il tutorial sarà valido sia per Linux che per Windows.

<h1>Installare git</h1>

Per poter utilizzare git avremo bisogno del comando git installato.
Per chi usa Windows  si può scaricare da: https://git-scm.com/download/win vi sconsiglio l'uso del portable se non sapete come gestire i programmi portable quanto ai comandi.
Per Linux: https://git-scm.com/download/linux potete seguire questa guida per tutte le varie distro (se la vostra distro non è inclusa si presuppone sappiate come muovervi di conseguenza)

<h1>Clonare la repo</h1>

Navighiamo alla cartella dove vogliamo posizionare i nostri file e:</br>
Se siamo su windows facciamo tasto destro --> Apri Git bash qua</br>
Se siete su linux --> Apri il terminale qua</br>

Nella shell che si aprirà digitate:</br>

`git clone https://gitlab.com/_Zaizen_/pecore-elettriche.git`</br>

Quest'operazione vi richiederà il login di GitLab in quanto è una repo privata. </br>
Se non vi richiede il login vi darà le istruzioni su come configurare un account system-wide oppuere per la cartella singola.</br>
Solitamente per configurare l'account i comandi sono:</br>

```
git config --global user.name "Il_vostro_username"
git config --global user.email la_vostra_email@esempio.com
```

Quando farete il clone vi richiederà comunque la password in quanto nella configurazione appena citata non la si può inserire.
Se vi è apparsa una nuova cartella chiamata pecore-elettriche, complimenti, avete clonato la repo con successo!

<h1>Inserire cose nella repo</h1>

Oltre a clonare in quanto mantenitori della repo potete anche modificare ciò che è presente. 
Nel terminale precedentemente aperto entriamo nella cartella con il comando:
`cd pecore-elettriche`
Per essere più comodi consiglio di aprire la cartella anche da interfaccia grafica (sempre che il vostro SO ne abbia una).
Ora che siamo dentro la cartella vedremo vari file uguali a quelli visibili dal sito più una cartella nascosta chiamata `.git`.
Quest'ultima non andrà **MAI** toccata.
Ora che siete nella cartella qualunque cosa che volete aggiungere alla repo dovrete metterla nella cartella.
Una volta inserito il file desiderato nella cartella si ritorna nella shell di prima dove abbiamo clonato la repo per mandare le modifiche fatte nella cartella al server.
Il procedimento si divide in 3 fasi:
1. Aggiungere i file
2. Commento sulle modifiche
3. Invio delle modifiche

**Aggiungere i file**

Se si vuole aggiungere tutte le modifiche fatte a ciò che verrà inviato al server si userà il comando:
`git add -A`
Se invece i file interessati non fossero tutti possiamo invece fare:
`git add nomefile`
E ripetere l'operazione per tutti i file desiderati
Oppure:
`git add nomefile1 nomefile2 nomefile3`

**Commento sulle modifiche**

In questa fase scriveremo cosa è cambiato da prima in modo tale da poter tener traccia di ciò che è stato fatto. 
Il comando per fare ciò è:
`git commit -m "Il mio messaggio su ciò che è stato fatto`

**Invio delle modifiche**

Ora che abbiamo fatto un commento sulle modifiche ed abbiamo aggiunto ciò che vogliamo mandare come modifiche dovremo inviarle.
Per fare ciò useremo:
`git push`

<h1>Ottenere nuove modifiche dalla repo</h1>

Spesso capiterà che a mandare le modifiche potrebbe esser stata un'altra persona e ciò porterebbe ad avere una versione sulla propria cartella nel pc non aggiornata. 
È quindi **necessario** aggiornare i file locali con quelli nuovi prima di iniziare a lavorare a qualunque cosa.
Per fare ciò si usa:
`git pull`





